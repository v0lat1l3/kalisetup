#!/bin/python3
import sys
import os
import subprocess
import psutil
import time
from datetime import datetime
import pyfiglet
import argparse
import socket
import struct

# This script (when started) sits and waits for a connection on eth0.
# That means for your Pi to be connected to an innocent ethernet jack ;)
# If that happens it checks if there is a connection to the network and carefully gathers info
# about this network. This info is stored in a folder in your home directory.
# Basically it allows you to check multiple jacks in a building and later review the results.
# It is recommended to have the correct system time in order to work with the created logfile.
# 
# written by v0lat1l3 2024.

# Please check if those options work for you:
ip_for_inet_test = "8.8.8.8" # Contacts Google DNS server to check intenet connection
nmap_options = "-PR" # Default setting for nmap is an ARP Scan (-PR)

# function to create a log file (There will be no command line output)
def create_log():
    print("[i] eth pr0be is running in logging mode! There will be not output in the command line.")
    # create a folder for the logs:
    log_dir = os.path.expanduser("~/Documents/eth_probe")
    
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
        print(f"[i] created folder {log_dir}")
    else:
        print(f"[i] using already existing folder {log_dir}")

    # start logging:
    now = datetime.now().strftime('%Y%m%d-%H%M')
    old_stdout = sys.stdout
    log_file = open(f"{log_dir}/{now}.log", "w")
    print(f"[i] created logfile {now}.log")
    sys.stdout = log_file
    check_ethernet()
    sys.stdout = old_stdout
    log_file.close()


def get_network_info():
    # Wait 10 seconds to allow DHCP to provide IP
    time.sleep(10)

    # Get network information
    ip_cmd = "ip addr show eth0 | grep 'inet ' | awk '{print $2}' | cut -d/ -f1"
    gateway_cmd = "ip route show | grep 'default via' | awk '{print $3}'"
    netmask_cmd = "ip addr show eth0 | grep 'inet ' | awk '{print $2}' | cut -d/ -f2"
    ip = subprocess.check_output(ip_cmd, shell=True).decode().strip()
    gateway = subprocess.check_output(gateway_cmd, shell=True).decode().strip()
    netmask = subprocess.check_output(netmask_cmd, shell=True).decode().strip()

    # Check internet connection
    ping_cmd = f"ping -c 1 {ip_for_inet_test} > /dev/null && echo 'connected' || echo 'NOT connected'"
    internet_connection = subprocess.check_output(ping_cmd, shell=True).decode().strip()

    print(f"[i] received IP {ip}")
    print(f"[i] gateway is {gateway}")
    print(f"[i] netmask is {netmask}")
    print(f"[i] internet: {internet_connection}")

    # perform an nmap scan
    nmap_scan(ip, netmask)


def nmap_scan(ip, netmask):
    # convert ip to 32 bit integer
    ip_int = struct.unpack("!I", socket.inet_aton(ip))[0]
    # calculate the subnet
    mask_int =(0xffffffff << (32 - int(netmask))) & 0xffffffff
    # subnetmask to dotted quad notation
    mask = socket.inet_ntoa(struct.pack("!I", mask_int))
    # get lowest ip in subnet
    lowest_ip_int = ip_int & mask_int
    lowest_ip = socket.inet_ntoa(struct.pack("!I", lowest_ip_int))
    # get subnet for nmap command
    nmap_target = f"{lowest_ip}/{netmask}"
    nmap_cmd = f"nmap {nmap_options} {nmap_target}"
    nmap_result = subprocess.check_output(nmap_cmd, shell=True).decode().strip()
    print(f"[i] output of nmap scan for network {nmap_target} with options {nmap_options}:\n{nmap_result}")

 
def check_ethernet():
    try:
        print("[i] waiting for connection on eth0...")
        connected = False
        while True:
            if psutil.net_if_stats().get('eth0').isup and not connected:
                now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                print(f"[i] {now} ethernet connected.")
                get_network_info()
                connected = True
            elif not psutil.net_if_stats().get('eth0').isup and connected:
                now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                print(f"[i] {now} ethernet disconnected.")
                connected = False
            time.sleep(1)

    # Allow the user to exit the script with Ctrl + C
    except KeyboardInterrupt:
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        print(f"\n[i] {now} Stopping the script")



# START #
banner = pyfiglet.figlet_format("eth_pr0be")
print(f"{banner}\n")

parser = argparse.ArgumentParser(description="script will listen to connections on eth0 and try to gather information from the network. Use Ctrl + C to exit.")
parser.add_argument("-l", "--create_log", action="store_true", help="script will run in logging mode. No cli output.")
args = parser.parse_args()

if args.create_log:
    create_log()    
else:
    check_ethernet()
