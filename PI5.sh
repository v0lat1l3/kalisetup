#!/bin/bash

# This script can be run on a fresh Kali installation on a Raspberry Pi5
# It is recommended to run sudo apt update && sudo apt full-upgrade -y && reboot before starting this script.
# However, some settings have to be done manually.
#
# Note: I use a external Wifi card with a MediaTek chip as the Realtek drivers caused too many issues on my Pi

packages=("kali-tools-802-11" "kali-tools-web" "kali-tools-windows-resources"\
 "kali-tools-social-engineering" "gobuster" "feroxbuster" "maryam" "yersinia" "flameshot" "wifiphisher" "rlwrap"  "seclists" "tldr" "plocate" "peass"\
 "code-oss" "keepassxc" "bettercap" "hostapd" "lighttpd" "beef-xss" "isc-dhcp-server" "hcxtools" "mdk4" "burpsuite"\
 "fastfetch" "htop" "wavemon" "youtubedl-gui" "id3v2" "kazam" "koadic" "jq" "tmux" "xrdp" "speedtest-cli")
install_packages(){
for package in "${packages[@]}";do
    echo "[+] Installing $package..."
    if sudo apt install -y "$package"; then
        echo "$package installed successfully."
    else
        notify_failure "$package"
    fi
done
}

# Function to notify in case of failure to install a certain package
notify_failure(){
    local failed_package="$1"
    echo "Failed to install $failed_package"
}


echo ""
echo '[+] Setting up metwork profiles.'

# Prompt for user input
read -p "Enter Hotspot Name (for Pi): " hotspot_name
read -p "Enter Hotspot Password (for Pi): " hotspot_passwd
read -p "Enter Phone Name (hotspot name of your phone): " phone_name
read -p "Enter Phone Password (hotspot password of your phone): " phone_passwd

# Define the file paths
hotspot_file="/etc/NetworkManager/system-connections/Hotspot.nmconnection"
client_file="/etc/NetworkManager/system-connections/Client.nmconnection"

# Create the Hotspot configuration file
sudo bash -c "cat > $hotspot_file <<EOL
[connection]
id=Hotspot
uuid=1244bed1-0938-4d90-9e9c-8c70f21c20a6
type=wifi
interface-name=wlan0
timestamp=1740264936

[wifi]
band=bg
mode=ap
ssid=$hotspot_name

[wifi-security]
group=ccmp
key-mgmt=wpa-psk
pairwise=ccmp
proto=rsn
psk=$hotspot_passwd

[ipv4]
address1=10.0.0.1/29
method=shared

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
EOL"

# Create the Client configuration file
sudo bash -c "cat > $client_file <<EOL
[connection]
id=Client
uuid=89e46fd7-5cce-41d4-8d45-285180d10a53
type=wifi
autoconnect=false
timestamp=1740262639

[wifi]
mode=infrastructure
ssid=$phone_name

[wifi-security]
key-mgmt=wpa-psk
psk=$phone_passwd

[ipv4]
method=auto

[ipv6]
addr-gen-mode=stable-privacy
method=auto

[proxy]
EOL"

# Change permissions, otherwise you will not see these network profiles in "Advanced Network Configuration"
sudo chmod 600 /etc/NetworkManager/system-connections/*.nmconnection
sudo systemctl restart NetworkManager
echo "[i] Configuration files created successfully."


echo ""
echo '[+] Creating swap.'
read -p "Enter the size of the swap file in megabytes (e.g., enter 4096 for 4GB): " swap_size_mb
echo '[+] Creating swapfile. This may take a while...'
sudo dd if=/dev/zero of=/.swapfile bs=1M count=$swap_size_mb
sudo mkswap -v1 /.swapfile
sudo chmod 600 /.swapfile
sudo swapon /.swapfile
echo "/.swapfile	none	swap	sw	0	0" | sudo tee -a /etc/fstab


echo ""
echo '[+] Copying own scripts'
sudo mv ~/kalisetup/Update /usr/local/bin/Update
chmod +x /usr/local/bin/Update
sudo mv ~/kalisetup/eth0_probe.py /usr/local/bin/eth0_probe
chmod +x /usr/local/bin/eth0_probe
sudo mv ~/kalisetup/UpdateScripts /usr/local/bin/UpdateScripts
chmod +x /usr/local/bin/UpdateScripts
sudo mv ~/kalisetup/CTF /usr/local/bin/CTF
chmod +x /usr/local/bin/CTF
sudo mv ~/kalisetup/switch_conn /usr/local/bin/switch_conn
chmod +x /usr/local/bin/switch_conn
sudo mv ~/kalisetup/fakeap /usr/local/bin/fakeap
chmod +x /usr/local/bin/fakeap


echo ""
echo '[+] Copying Wallpapers'
unzip ~/kalisetup/Pictures.zip
sudo mv ~/kalisetup/Pictures/Anonymous.jpeg /usr/share/backgrounds/kali-16x9/Anonymous.jpeg
# TODO: add more pictures here / Add Zip Folder to repo
rm ~/kalisetup/Pictures.zip
rm -fr ~/kalisetup/Pictures


echo ""
echo "[+] Installing Mullvad VPN Client. This might take a while..."
cd ~/Downloads
wget --content-disposition https://mullvad.net/de/download/app/arm-deb/latest
sudo apt install -y ./MullvadVPN-*.deb
rm MullvadVPN-*
cd ~/


echo ""
echo '[+] Setting up Metaslpoit Framework database'
sudo systemctl start postgresql.service
sudo systemctl enable postgresql
sudo msfdb init


echo ''
echo '[+] Change SSH keys as all ARM images are pre-configured with the same keys.'
sudo rm /etc/ssh/ssh_host_*
sudo dpkg-reconfigure openssh-server
sudo service ssh restart


echo ""
echo '[+] Disabling unneccessary services and speed up the boot process'
# Just disabling the following service did not work for me, so I masked it.
#sudo systemctl disable systemd-networkd-wait-online.service
sudo systemctl mask systemd-networkd-wait-online.service 
sudo systemctl disable cloud-init-main
sudo systemctl disable NetworkManager-wait-online.service
# I keep these service enabled because they do not seem to slow down the boot process
#sudo systemctl disable plocate-updatedb.service
#sudo systemctl disable plocate-updatedb.timer
# This can further speed up the boot process, but may cause instability issues:
#echo "" | sudo tee -a /boot/config.txt
#echo "# Options to speed up the boot process - Created by postinstall script:" | sudo tee -a /boot/config.txt
#echo "disable_splash=1" | sudo tee -a /boot/config.txt
#echo "boot_delay=0" | sudo tee -a /boot/config.txt


echo ''
echo '[+] Adding new aliases to .zshrc (some others will be added later for tools downloaded from GITHUB)...'
echo "" >> ~/.zshrc
echo "### additional aliases ###" >> ~/.zshrc
echo "alias co='sudo chown -R \$USER'" >> ~/.zshrc
echo "alias off='sudo shutdown -h now'" >> ~/.zshrc
echo "alias monup='sudo airmon-ng start wlan1'" >> ~/.zshrc
echo "alias kill_hotspot='sudo nmcli connection down "Hotspot"'" >> ~/.zshrc
echo "alias bt_on='sudo systemctl start bluetooth'" >> ~/.zshrc
echo "alias bt_off='sudo systemctl stop bluetooth'" >> ~/.zshrc
echo "alias get_temp='watch sudo vcgencmd measure_temp'" >> ~/.zshrc
echo "alias mdk4_random='sudo mdk4 wlan1mon b -a -w nta -m'" >> ~/.zshrc
echo "alias clear_history='echo "" > ~/.zsh_history'" >> ~/.zshrc
echo "alias mount_sda1='sudo mkdir -p /media/usb; sudo mount /dev/sda1 /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias mount_sda='sudo mkdir -p /media/usb; sudo mount /dev/sda /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias unmountUsb='cd; sudo umount -f /media/usb'" >> ~/.zshrc
# Add more if you like :)


echo ""
echo '[+] Installing additional packages'
install_packages


echo ''
echo '[+] Enabling RDP.'
sudo adduser xrdp ssl-cert
sudo systemctl enable xrdp
sudo adduser rmdt
sudo usermod -aG sudo rmdt


echo ''
echo '[+] Finishing the setup'
sudo updatedb
tldr -u
sudo apt autoremove -y
sudo apt autoclean
sudo apt clean
echo ""
echo 'All done! - You can remove the "kalisetup" directory now.'
echo "[!] Change the default shell of the remote user to /usr/bin/zsh and write the additional aliases to his .zshrc!"
