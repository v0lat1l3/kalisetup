# Use this script to switch between hotspot mode and client mode.
# You have to create a wifi connection in client mode named "Client" with the credentials of your phones hotspot
# and another connection named "Hotspot" that starts a hotspot on the Pi.
# For me the hotspot mode is the default setting on the Pi and I switch to client mode to access the internet when no
# WiFi is nearby.

#!/bin/bash

# Check if the hotspot is active
if nmcli connection show --active | grep -q "Hotspot"; then
    echo "[i] Disabling hotspot on Pi and switching to client mode."
    echo "[i] Make sure that your phones hotspot is enabled before you continue..."
    
    nmcli connection down "Hotspot"
    
    # Check the exit status of the previous command
    if [ $? -eq 0 ]; then
        sleep 10  # Wait for a few seconds to ensure the hotspot is down
        # Enable client mode
        nmcli connection up "Client"
    else
        # Notify user if the hotspot could not be disabled
        echo "[!] Failed to disable hotspot mode."
    fi

# If Pi is already in client mode, switch back to hotspot mode.    
else
    echo "[i] Switching back to hotspot mode."
    echo "[i] This session will die soon... Please reconnect to the Pi hotspot."
    
    nmcli connection down "Client"
    
    # Check the exit status of the previous command
    if [ $? -eq 0 ]; then
        sleep 10 
        # Enable hotspot mode
        nmcli connection up "Hotspot"
    else
        # Notify user if the hotspot could not be disabled
        echo "[!] Failed to disable client mode."
    fi    
fi
