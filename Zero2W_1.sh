#!/bin/bash

# This script can be run on a fresh Kali installation on a Raspberry Pi Zero 2 W
# Run this script first, as it creates a swap file that make the subsequent update process more effective.
# 512 MB RAM is simply not enough.


echo ""
echo '[+] Creating swapfile. This may take a while...'
sudo dd if=/dev/zero of=/.swapfile bs=1M count=2048
sudo mkswap -v1 /.swapfile
sudo chmod 600 /.swapfile
sudo swapon /.swapfile
echo "/.swapfile	none	swap	sw	0	0" | sudo tee -a /etc/fstab

echo ""
echo '[+] Disabling unneccessary services and speed up the boot process'
sudo systemctl disable systemd-networkd-wait-online.service 
#sudo systemctl disable cloud-init-main
#sudo systemctl disable plocate-updatedb.service
#sudo systemctl disable plocate-updatedb.timer
#sudo systemctl disable NetworkManager-wait-online.service
#echo "" | sudo tee -a /boot/config.txt
#echo "# Options to speed up the boot process - Created by postinstall script:" | sudo tee -a /boot/config.txt
#echo "disable_splash=1" | sudo tee -a /boot/config.txt
#echo "boot_delay=0" | sudo tee -a /boot/config.txt

echo ""
echo '[+] Updating the system... This may take a while...'
sudo apt update
sudo apt full-upgrade -y
sudo apt autoremove

echo ""
echo '[+] Reboot now and run script 2 (Zero2W_2.sh)'