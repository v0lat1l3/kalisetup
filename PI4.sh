#!/bin/bash

# This script can be run on a fresh Kali installation on a Raspberry Pi4
# It is recommended to run sudo apt update && sudo apt full-upgrade -y && reboot before starting this script.
# However, some settings have to be done manually.
#
# Note: I use a external Wifi card with a MediaTek chip as the Realtek drivers caused too many issues on my Pi
# Note: On the Pi4 I run the 32 bit version of Kali. As a 32 bit OS can just use 4GB of RAM it is up to you to decide wether or not you want a swapfile.
# If so copy and paste the commands from the Pi 2 Zero or PI5 scripts.

packages=("kali-tools-802-11" "kali-tools-web" "kali-tools-windows-resources"\
 "kali-tools-social-engineering" "gobuster" "feroxbuster" "maryam" "yersinia" "flameshot" "wifiphisher" "rlwrap"  "seclists" "tldr" "plocate" "peass"\
 "code-oss" "keepassxc" "bettercap" "hostapd" "lighttpd" "beef-xss" "isc-dhcp-server" "hcxtools" "mdk4" \
 "fastfetch" "htop" "wavemon" "youtubedl-gui" "id3v2" "kazam" "koadic" "jq" "tmux" "xrdp" "speedtest-cli")
install_packages(){
for package in "${packages[@]}";do
    echo "[+] Installing $package..."
    if sudo apt install -y "$package"; then
        echo "$package installed successfully."
    else
        notify_failure "$package"
    fi
done
}

# Function to notify in case of failure to install a certain package
notify_failure(){
    local failed_package="$1"
    echo "Failed to install $failed_package"
}


echo ""
echo '[+] Copying own scripts'
sudo mv ~/kalisetup/Update /usr/local/bin/Update
chmod +x /usr/local/bin/Update
sudo mv ~/kalisetup/eth0_probe.py /usr/local/bin/eth0_probe
chmod +x /usr/local/bin/eth0_probe
sudo mv ~/kalisetup/UpdateScripts /usr/local/bin/UpdateScripts
chmod +x /usr/local/bin/UpdateScripts
sudo mv ~/kalisetup/CTF /usr/local/bin/CTF
chmod +x /usr/local/bin/CTF
sudo mv ~/kalisetup/switch_conn /usr/local/bin/switch_conn
chmod +x /usr/local/bin/switch_conn
sudo mv ~/kalisetup/fakeap /usr/local/bin/fakeap
chmod +x /usr/local/bin/fakeap


echo ""
echo '[+] Copying Wallpapers'
unzip ~/kalisetup/Pictures.zip
sudo mv ~/kalisetup/Pictures/Anonymous.jpeg /usr/share/backgrounds/kali-16x9/Anonymous.jpeg
# TODO: add more pictures here / Add Zip Folder to repo
rm ~/kalisetup/Pictures.zip
rm -fr ~/kalisetup/Pictures


echo ""
echo '[+] Setting up Metaslpoit Framework database'
sudo systemctl start postgresql.service
sudo systemctl enable postgresql
sudo msfdb init


echo ''
echo '[+] Change SSH keys as all ARM images are pre-configured with the same keys.'
sudo rm /etc/ssh/ssh_host_*
sudo dpkg-reconfigure openssh-server
sudo service ssh restart


echo ""
echo '[+] Disabling unneccessary services and speed up the boot process'
sudo systemctl disable systemd-networkd-wait-online.service 
#sudo systemctl disable cloud-init-main
#sudo systemctl disable plocate-updatedb.service
#sudo systemctl disable plocate-updatedb.timer
#sudo systemctl disable NetworkManager-wait-online.service
#echo "" | sudo tee -a /boot/config.txt
#echo "# Options to speed up the boot process - Created by postinstall script:" | sudo tee -a /boot/config.txt
#echo "disable_splash=1" | sudo tee -a /boot/config.txt
#echo "boot_delay=0" | sudo tee -a /boot/config.txt


echo ''
echo '[+] Adding new aliases to .zshrc (some others will be added later for tools downloaded from GITHUB)...'
echo "" >> ~/.zshrc
echo "### additional aliases ###" >> ~/.zshrc
echo "alias co='sudo chown -R \$USER'" >> ~/.zshrc
echo "alias off='sudo shutdown -h now'" >> ~/.zshrc
echo "alias monup='sudo airmon-ng start wlan1'" >> ~/.zshrc
echo "alias kill_hotspot='sudo nmcli connection down "Hotspot"'" >> ~/.zshrc
echo "alias bt_on='sudo systemctl start bluetooth'" >> ~/.zshrc
echo "alias bt_off='sudo systemctl stop bluetooth'" >> ~/.zshrc
echo "alias get_temp='watch sudo vcgencmd measure_temp'" >> ~/.zshrc
echo "alias mdk4_random='sudo mdk4 wlan1mon b -a -w nta -m'" >> ~/.zshrc
echo "alias clear_history='echo "" > ~/.zsh_history'" >> ~/.zshrc
echo "alias mount_sda1='sudo mkdir -p /media/usb; sudo mount /dev/sda1 /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias mount_sda='sudo mkdir -p /media/usb; sudo mount /dev/sda /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias unmountUsb='cd; sudo umount -f /media/usb'" >> ~/.zshrc
# Add more if you like :)


echo ""
echo '[+] Installing additional packages'
install_packages


echo ''
echo '[+] Enabling RDP.'
sudo adduser xrdp ssl-cert
sudo systemctl enable xrdp
sudo adduser rmdt
sudo usermod -aG sudo rmdt

echo ''
echo '[+] Finishing the setup'
sudo updatedb
tldr -u
sudo apt autoremove -y
sudo apt autoclean
sudo apt clean
echo ""
echo 'All done! - You can remove the "kalisetup" directory now.'
echo '[!] Do not forget to login as remote user (rmdt) once before using RDP!'
echo "[!] If not done yet change hostname and password"
echo "[!] Change the default shell of the remote user to /usr/bin/zsh and write the additional aliases to his .zshrc!"