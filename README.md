# Kalisetup


## Description
<br> This repository contains scripts and files that I use to setup my Kali Linux machines.
Different Raspberry Pi models have their own scripts. For the Pi Zero 2 W two scripts are used to account for the minimalistic RAM.
I make some adjustments from the GUI (e.g. creating the WiFi profiles "Hotspot" and "Client", changing the keyboard layout, power settings,...)
after booting the Pi the first time when it is connected to a screen and keyboard. These changes are not mentioned here.
<br/>