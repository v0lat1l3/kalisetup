#!/bin/bash

# This script can be run on a fresh Kali VM installation
# Feel free to make changes.
# It is recommended to run sudo apt update && sudo apt full-upgrade -y && reboot before starting this script.
# However, some settings have to be done manually.

packages=("realtek-rtl88xxau-dkms" "qemu-guest-agent" "spice-vdagent" "spice-vdagent" "kali-tools-802-11" "kali-tools-web" "kali-tools-windows-resources"\
 "kali-tools-social-engineering" "gobuster" "feroxbuster" "maryam" "yersinia" "flameshot" "wifiphisher" "rlwrap"  "seclists" "tldr" "plocate" "peass" "chisel"\
 "code-oss" "keepassxc" "bettercap" "hostapd" "lighttpd" "beef-xss" "isc-dhcp-server" "hcxtools" "mdk4" \
 "ligolo-ng" "fastfetch" "htop" "wavemon" "youtubedl-gui" "id3v2" "torbrowser-launcher" "kazam" "koadic" "jq")
install_packages(){
for package in "${packages[@]}";do
    echo "[+] Installing $package..."
    if sudo apt install -y "$package"; then
        echo "$package installed successfully."
    else
        notify_failure "$package"
    fi
done
}

# Function to notify in case of failure to install a certain package
notify_failure(){
    local failed_package="$1"
    echo "Failed to install $failed_package"
}

echo ""
echo '[+] Copying own scripts'
sudo mv ~/kalisetup/Update /usr/local/bin/Update
chmod +x /usr/local/bin/Update
sudo mv ~/kalisetup/recon /usr/local/bin/recon
chmod +x /usr/local/bin/recon
sudo mv ~/kalisetup/eth0_probe.py /usr/local/bin/eth0_probe
chmod +x /usr/local/bin/eth0_probe
sudo mv ~/kalisetup/UpdateScripts /usr/local/bin/UpdateScripts
chmod +x /usr/local/bin/UpdateScripts
sudo mv ~/kalisetup/CTF /usr/local/bin/CTF
chmod +x /usr/local/bin/CTF

echo ""
echo '[+] Copying Wallpapers'
unzip ~/kalisetup/Pictures.zip
sudo mv ~/kalisetup/Pictures/Anonymous.jpeg /usr/share/backgrounds/kali-16x9/Anonymous.jpeg
# TODO: add more pictures here / Add Zip Folder to repo
rm ~/kalisetup/Pictures.zip
rm -fr ~/kalisetup/Pictures

echo ""
echo '[+] Setting up Metaslpoit Framework database'
sudo systemctl start postgresql.service
sudo systemctl enable postgresql
sudo msfdb init

echo ''
echo '[+] Adding new aliases to .zshrc (some others will be added later for tools downloaded from GITHUB)...'
echo "" >> ~/.zshrc
echo "### additional aliases ###" >> ~/.zshrc
echo "alias co='sudo chown -R \$USER'" >> ~/.zshrc
echo "alias off='sudo shutdown -h now'" >> ~/.zshrc
echo "alias monup='sudo airmon-ng start wlan0'" >> ~/.zshrc
echo "alias kill_hotspot='sudo nmcli connection down "Hotspot"'" >> ~/.zshrc
echo "alias bt_on='sudo systemctl start bluetooth'" >> ~/.zshrc
echo "alias bt_off='sudo systemctl stop bluetooth'" >> ~/.zshrc
echo "alias get_temp='watch sudo vcgencmd measure_temp'" >> ~/.zshrc
echo "alias mdk4_random='sudo mdk4 wlan0mon b -a -w nta -m'" >> ~/.zshrc
echo "alias clear_history='echo "" > ~/.zsh_history'" >> ~/.zshrc
echo "alias mount_sda1='sudo mkdir -p /media/usb; sudo mount /dev/sda1 /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias mount_sda='sudo mkdir -p /media/usb; sudo mount /dev/sda /media/usb; cd /media/usb'" >> ~/.zshrc
echo "alias unmountUsb='cd; sudo umount -f /media/usb'" >> ~/.zshrc
# Add more if you like :)

echo ""
echo '[+] Installing additional packages'
install_packages

echo ""
echo "[+] Installing rustscan"
wget https://github.com/RustScan/RustScan/releases/download/2.3.0/rustscan_2.3.0_amd64.deb
sudo dpkg -i  rustscan_2.3.0_amd64.deb
rustscan --version
rm rustscan_2.3.0_amd64.deb

echo ''
echo '[+] Finishing the setup'
sudo updatedb
tldr -u
sudo apt autoremove -y
sudo apt autoclean
sudo apt clean
echo 'All done! - You can remove the "kalisetup" directory now.'
